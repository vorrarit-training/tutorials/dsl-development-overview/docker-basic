FROM adoptopenjdk/openjdk11:alpine-jre
RUN apk add --no-cache tzdata
RUN addgroup -S spring --gid 1000 && adduser -S spring -G spring --uid 1000
USER 1000:1000
ENV TZ=Asia/Bangkok
ENV DEBIAN_FRONTEND=noninteractive
COPY target/dsl-simple-backend-0.0.1-SNAPSHOT.jar /dsl-simple-backend-0.0.1-SNAPSHOT.jar
ENTRYPOINT exec java -jar /dsl-simple-backend-0.0.1-SNAPSHOT.jar --spring.config.location=/home/application.properties